/*
    global jogo
    global Phaser
    global verdade
    global falso
    global Welpworld
*/

Welpworld.Game = function() {
 
    this.velocidadeJogador = 300;
    this.velocidadeBalas = 500;

    this.frequenciaBala=500;
    this.proximaBala=0;

    this.frequenciaBomba1=2000;
    this.proximaBomba1=jogo.numeroAleatorio(2000,6000);
    
    this.frequenciaBomba2=1000;
    this.proximaBomba2=jogo.numeroAleatorio(2000,6000);
    
    this.frequenciaBomba3=3000;
    this.proximaBomba3=jogo.numeroAleatorio(2000,6000);

    this.movimentoMaximoX=100;  

    this.vivo=verdade;
    this.reiniciar = falso;

    this.pontos = 0;
    this.vidas = 7;
};

Welpworld.Game.prototype = {
    create: function() {
    
        this.fundo = jogo.utilizarSprite(-1,-1,jogo.LARGURA,jogo.ALTURA,'fundo');
        this.chao = jogo.utilizarSprite(-1,jogo.ALTURA-70,jogo.LARGURA,jogo.ALTURA,'chao');

    
        this.nuvens = jogo.utilizarSprite(-1,-1,jogo.LARGURA,190,'nuvens');
        jogo.rotacaoSprite(this.nuvens,-10,0);
    
        this.tanque = jogo.utilizarImagem(jogo.CENTRO_X, 375, 0.5 , 'tanque');
        jogo.escala(this.tanque,0.5);

        this.canhao = jogo.utilizarImagem(jogo.CENTRO_X-3, 375-10 ,0, 'canhao');
        jogo.ancora(this.canhao,-0.1,0.5);
        jogo.escala(this.canhao,0.5);
    
        this.helicoptero1 = jogo.utilizarImagem(-200, 50 , 0.5, 'helicoptero');
        jogo.escala(this.helicoptero1,0.5);
        jogo.criarAnimacao("rodar",[0,1,2,3],this.helicoptero1);
        jogo.iniciarAnimacao('rodar',15,jogo.verdade,this.helicoptero1);


        this.helicoptero2 = jogo.utilizarImagem(1400, 50 , 0.5, 'helicoptero');
        jogo.escala(this.helicoptero2,0.5);
        jogo.criarAnimacao("rodar",[0,1,2,3],this.helicoptero2);
        jogo.iniciarAnimacao('rodar',15,jogo.verdade,this.helicoptero2);

        this.helicoptero3 = jogo.utilizarImagem(-500, 50 , 0.5, 'helicoptero');
        jogo.escala(this.helicoptero3,0.5);
        jogo.criarAnimacao("rodar",[0,1,2,3],this.helicoptero3);
        jogo.iniciarAnimacao('rodar',15,jogo.verdade,this.helicoptero3);

        this.balas = jogo.novoGrupo();
        this.bombas = jogo.novoGrupo();
        
        jogo.utilizarFisicaGrupo(this.balas);
        jogo.criarVarios(this.balas, 5, 'bala');
        jogo.definirParaTodos(this.balas, 'verificarLimitesTela',jogo.verdade);
        jogo.definirParaTodos(this.balas, 'destruirForaTela', jogo.verdade);
        
        jogo.utilizarFisicaGrupo(this.bombas);
        jogo.criarVarios(this.bombas, 15, 'bomba');
        jogo.definirParaTodos(this.bombas, 'verificarLimitesTela',jogo.verdade);
        jogo.definirParaTodos(this.bombas, 'destruirForaTela', jogo.verdade);

    

        jogo.activarFisica();

        jogo.utilizarFisica(this.chao);
        jogo.utilizarFisica(this.tanque);
        jogo.utilizarFisica(this.canhao);
        jogo.utilizarFisica(this.helicoptero1);
        jogo.utilizarFisica(this.helicoptero2);
        jogo.utilizarFisica(this.helicoptero3);
        
        jogo.colidirComLimites(this.tanque);
        jogo.colidirComLimites(this.canhao);

        this.textoPontos = jogo.adicionarTextoBitmap(25,405,'minecraftia',"Pontuacao: " + this.pontos,14);
        this.textoVidas =  jogo.adicionarTextoBitmap(jogo.LARGURA-90,405,'minecraftia',"Vidas: " + this.vidas, 14);
        
    },
    update: function() {

        jogo.sobreposicao(this.balas, this.bombas, this.balaColideBomba, this);
        jogo.sobreposicao(this.bombas, this.chao, this.bombaColideChao, this);

        if(jogo.tempo()>this.proximaBala && jogo.jogo.input.activePointer.isDown){
        this.criarBala();
        this.proximaBala = jogo.tempo() + this.frequenciaBala; 
        }

    if(jogo.tempo()>this.proximaBomba1 && this.helicopteroPodeDisparar(this.helicoptero1)){
        this.criarBomba(this.helicoptero1.x,this.helicoptero1.y);
        this.proximaBomba1 = jogo.tempo() + this.frequenciaBomba1; 
        this.frequenciaBomba1 = jogo.numeroAleatorio(1000,4000);
        }
    if(jogo.tempo()>this.proximaBomba2 && this.helicopteroPodeDisparar(this.helicoptero2)){
        this.criarBomba(this.helicoptero2.x,this.helicoptero2.y);
        this.proximaBomba2 = jogo.tempo() + this.frequenciaBomba2; 
        this.frequenciaBomba2 = jogo.numeroAleatorio(1000,4000);
        
        }
    if(jogo.tempo()>this.proximaBomba3 && this.helicopteroPodeDisparar(this.helicoptero3)){
        this.criarBomba(this.helicoptero3.x,this.helicoptero3.y);
        this.proximaBomba3 = jogo.tempo() + this.frequenciaBomba3; 
        this.frequenciaBomba3 = jogo.numeroAleatorio(1000,4000);
        
        }

        this.movimentoTanque();

        this.movimentoHelicoptero(this.helicoptero1);
        this.movimentoHelicoptero(this.helicoptero2);
        this.movimentoHelicoptero(this.helicoptero3);

    

        if((jogo.teclaPressionada("enter") || jogo.jogo.input.activePointer.isDown) && this.reiniciar===jogo.verdade)
        this.recomecar();

    },
    
    movimentoTanque: function() {

        if(jogo.teclaPressionada("esquerda") && this.tanque.x>this.movimentoMaximoX){
        jogo.definirVelocidadeX(this.tanque,-this.velocidadeJogador);
        jogo.definirVelocidadeX(this.canhao,-this.velocidadeJogador);
        }else if(jogo.teclaPressionada("direita") && this.tanque.x<jogo.larguraTela()-this.movimentoMaximoX){
        jogo.definirVelocidadeX(this.tanque,this.velocidadeJogador);
        jogo.definirVelocidadeX(this.canhao,this.velocidadeJogador);
        }else{ 
        jogo.definirVelocidadeX(this.tanque,0);
        jogo.definirVelocidadeX(this.canhao,0);
        }

        jogo.seguirApontador(this.canhao);
    },
    movimentoHelicoptero: function(helicoptero){
        if(helicoptero.x<-100){
            var y = jogo.numeroAleatorio(25,100);
            var velocidadeX = jogo.numeroAleatorio(50,150);
            
            jogo.posicao(helicoptero,-100,y);
            jogo.velocidadeX(helicoptero,velocidadeX);
            jogo.espelhar(helicoptero,'direita');
        }else if(helicoptero.x>jogo.LARGURA+100){
            var y = jogo.numeroAleatorio(25,100);
            var velocidadeX = jogo.numeroAleatorio(-50,-150);
            
            jogo.definirPosicao(helicoptero,jogo.larguraTela()+100,y);
            jogo.definirVelocidadeX(helicoptero,velocidadeX);
            jogo.espelharSprite(helicoptero,'esquerda');
        } 
    },
    helicopteroPodeDisparar: function(helicoptero){
        if(helicoptero.x>35 && helicoptero.x<jogo.LARGURA-35 && jogo.objetosDestruidos(this.bombas) > 0)
            return verdade;
        else
            return falso;
    },
    criarBala: function(){

        var bala = jogo.primeiroElementoDestruido(this.balas);
        jogo.ancora(bala,0.5,0.5);
        
        jogo.definirAngulo(bala,jogo.angulo(this.canhao));

        jogo.posicao(bala, this.canhao.x, this.canhao.y);
        jogo.velocidadeAngular(bala,600);
    },
    criarBomba: function(x,y){

        var bomba = jogo.primeiroElementoDestruido(this.bombas);
        jogo.ancora(bomba,0.5,0.5);
        jogo.posicao(bomba,x,y);
        jogo.velocidadeY(bomba,50);

    },

    balaColideBomba: function(bala,bomba){

        jogo.destruirObjecto(bala);
        jogo.destruirObjecto(bomba);

        this.pontos = this.pontos + 5;
        jogo.alterarTexto(this.textoPontos,'Pontuacao: '+ this.pontos);
    },
    bombaColideChao: function(bomba,chao) {

        var explosao = jogo.utilizarImagem(chao.x,chao.y,0.5,'explosao');
        jogo.criarAnimacao('explodir',[0,1,2,3,4,5,6],explosao);
        jogo.iniciarAnimacao('explodir',20,falso,explosao,verdade);

        jogo.destruirObjecto(chao);

        if(this.vidas===0){
            this.fimJogo();
        }else{
            this.vidas = this.vidas - 1;
            jogo.alterarTexto(this.textoVidas, 'Vidas: ' + this.vidas);
        }
        
    },
    fimJogo: function() {
        
        jogo.destruirGrupo(this.bombas);     
        this.vivo=falso;

        jogo.retangulo(0,0,jogo.larguraTela(),jogo.alturaTela(),'#000',0.7);
        
        var texto="O pontuacao foi: " + this.pontos; 
        var pontuacao=jogo.adicionarTextoBitmap(0,0,'minecraftia', texto, 28);
        pontuacao.x = jogo.CENTRO_X - jogo.centroX(pontuacao) ;
        pontuacao.y = jogo.CENTRO_Y - jogo.centroY(pontuacao) - 25;
        
        texto = '(Pressiona enter para reiniciar)';
        var reiniciar=jogo.adicionarTextoBitmap(0,0,'minecraftia', texto,12);
        reiniciar.x = jogo.CENTRO_X - jogo.centroX(reiniciar) ;
        reiniciar.y = jogo.CENTRO_Y - jogo.centroY(reiniciar) + 50;

        this.reiniciar=verdade;
    },
    recomecar: function() {
        this.reiniciar = falso;
        this.proximoTiro = 0;
        this.pontos=0;
        this.vidas=7;
        this.proximaBomba1=0;
        this.proximaBomba2=0;
        this.proximaBomba3=0;

        this.vivo = verdade;
        jogo.activarEstado('Game');
    }
}