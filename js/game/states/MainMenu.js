/*
    global jogo
    global Phaser
    global verdade
    global falso
    global Welpworld
*/

Welpworld.MainMenu = function() {};

Welpworld.MainMenu.prototype = {
    create: function() {
   
        this.fundo = jogo.utilizarSprite(-1,-1,jogo.LARGURA,jogo.ALTURA,'fundo');
    
        this.nuvens = jogo.utilizarSprite(-1,-1,jogo.LARGURA,190,'nuvens');
        jogo.rotacaoImagem(this.nuvens,-10,0);

        var texto = 'Carrega ENTER para iniciar o jogo';
        var pontuacao=jogo.adicionarTextoBitmap(0,0,'minecraftia', texto,24);
        pontuacao.x = jogo.CENTRO_X - jogo.centroX(pontuacao);
        pontuacao.y = jogo.CENTRO_Y - jogo.centroY(pontuacao) - 75;


        var texto = 'o rato  move o canhao e dispara';
        pontuacao=jogo.adicionarTextoBitmap(0,0,'minecraftia', texto,16);
        pontuacao.x = jogo.CENTRO_X - jogo.centroX(pontuacao);
        pontuacao.y = jogo.CENTRO_Y - jogo.centroY(pontuacao) - 25;

        var texto = 'setas fazem o tanque andar';
        pontuacao=jogo.adicionarTextoBitmap(0,0,'minecraftia', texto,16);
        pontuacao.x = jogo.CENTRO_X - jogo.centroX(pontuacao);
        pontuacao.y = jogo.CENTRO_Y - jogo.centroY(pontuacao);
        
    },
    update: function() {
        if(jogo.teclaPressionada('enter') || jogo.jogo.input.activePointer.isDown){
            jogo.activarEstado('Game');
        }
    }
    
};