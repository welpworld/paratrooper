/*
    global jogo
    global Phaser
    global verdade
    global falso
    global Welpworld
*/

Welpworld.Preload = function() {
  this.pronto = jogo.falso;
  
};

Welpworld.Preload.prototype = {
  preload: function() {

    this.splash =jogo.utilizarImagem(jogo.CENTRO_X, jogo.CENTRO_Y, 0.5, 'logo');
  
    this.barra = jogo.utilizarImagem(jogo.CENTRO_X, jogo.CENTRO_Y + 150, 0.5, 'preloadbar');
    
    jogo.barraCarregamento(this.barra);
 
    jogo.carregarImagem('fundo', 'assets/images/fundo-azul.png');
    jogo.carregarImagem('chao', 'assets/images/chao.png');

    jogo.carregarImagem('nuvens', 'assets/images/nuvens.png', 106, 93, 1);

    jogo.carregarImagem('tanque', 'assets/images/tanque_base.png');
    jogo.carregarImagem('canhao', 'assets/images/tanque_cano.png');
    jogo.carregarImagem('bala', 'assets/images/bala_tanque.png');
    
    jogo.carregarSprite('helicoptero', 'assets/images/aviao_inimigo.png',146,57,4);
    jogo.carregarImagem('bomba', 'assets/images/bala_inimigo.png');
    jogo.carregarSprite('explosao', 'assets/images/explosao.png',68,67,6);

    
    jogo.carregarTextoBitmap('minecraftia', 'assets/fonts/minecraftia/minecraftia.png', 'assets/fonts/minecraftia/minecraftia.xml');

    //This needs something to load or won't be called'
    jogo.carregamentoCompleto(this.carregamentoCompleto,this);
  },
  
  update: function() {
   if( this.pronto === verdade ){
   jogo.activarEstado('MainMenu');
    }
  },
 
  carregamentoCompleto: function() {
    this.pronto = verdade;
  },

 
};