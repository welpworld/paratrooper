/*
    global jogo
    global Phaser
    global verdade
    global falso
*/
  
var Welpworld = function() {};

Welpworld.Boot = function() {};

Welpworld.Boot.prototype = {
    preload: function() {

        jogo.carregarImagem('logo', 'assets/images/logo.png');
        jogo.carregarImagem('preloadbar', 'assets/images/preloader-bar.png');
    
        jogo.jogo.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        jogo.forcarOrientacao(verdade, falso);

        jogo.orientacaoCorrecta(this.handleCorrect());
        jogo.orientacaoIncorrecta(this.handleIncorrect());
    
    },
    create: function() {
        jogo.fundo('#FFF');
        
        //  Unless you specifically know your game needs to support multi-touch I would recommend setting this to 1
        jogo.numeroToques(1);

        jogo.capturarTecla("cima");
        jogo.capturarTecla("baixo");
        jogo.capturarTecla("direita");
        jogo.capturarTecla("esquerda");
        jogo.capturarTecla("espaco");
        
        if (jogo.paraDispositivoMovel()) {
        jogo.definirDimensoesMovel(500,300,1100,600,true);
        }

        jogo.activarEstado('Preloader');
    },
    handleIncorrect:function(){
        if(jogo.dispositivoMovel()){
            document.getElementById('turn').style.display='block';
        }
    },
	handleCorrect:function(){
		if(jogo.dispositivoMovel()){
            document.getElementById('turn').style.display='none';
		}
	}
};